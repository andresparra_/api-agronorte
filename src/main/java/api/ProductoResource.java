package api;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import java.util.List;
import modelo.dao.ProductoDao;
import modelo.entity.Producto;

/**
 *
 * @author andresparra
 */
@Path("/productos")
public class ProductoResource {
    private ProductoDao repositorio = new ProductoDao();
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Producto> getTiendas() {
        return repositorio.consultar();
    }
}
