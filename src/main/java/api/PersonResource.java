package api;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

/**
 *
 * @author andrev
 */
@Path("/person")
public class PersonResource {
    @GET
    public String getName() {
        return "Andres";
    }
}
