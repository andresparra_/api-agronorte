package api;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.List;
import modelo.dao.TiendaDao;
import modelo.entity.Tienda;

@Path("/apitiendas")
public class TiendaResource {
    
    private TiendaDao repositorio = new TiendaDao();
    
    @GET
    @Path("/ping")
    public Response ping() {
        return Response.ok().entity("Service online tiendas").build();
    }
    
    @GET
    @Path("/tiendas/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Tienda getTienda(@PathParam("id") String id) {
        Tienda tienda = new Tienda(id);
        return repositorio.consultarId(tienda);
    }

    @GET
    @Path("/tiendas/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Tienda> getTiendas() {
        return repositorio.consultar();
    }
    
    
    @POST
    @Path("/tiendas/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response post(Tienda tienda)
    {
        try{
            repositorio.insertar(tienda);
            return Response.status(Response.Status.CREATED).entity(tienda).build();
        }
        catch(Exception ex)
        {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        } 
    }
}