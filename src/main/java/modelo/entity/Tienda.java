/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.entity;

/**
 *
 * @author IMAGEN-02
 */
public class Tienda {
    private String nit;
    private String nombre;
    private double latitud;
    private double longitud;
    private String telefono;
    private String descripcion;
    
    public Tienda(String nit) {
        this.nit = nit;
    }

    public Tienda(String nit, String nombre, double latitud, double longitud, String telefono, String descripcion) {
        this.nit = nit;
        this.nombre = nombre;
        this.latitud = latitud;
        this.longitud = longitud;
        this.telefono = telefono;
        this.descripcion = descripcion;
    }

    public Tienda() {
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "Tienda{" + "nit=" + nit + ", nombre=" + nombre + ", latitud=" + latitud + ", longitud=" + longitud + ", telefono=" + telefono + ", descripcion=" + descripcion + '}';
    }
    
    
    
    
}
