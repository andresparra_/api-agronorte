package modelo.entity;

public class Producto {
    private String nombre;
    private double precio;
    private int cantidadStock;
    private String descripcion;
    private String referencia;

    public Producto() {
    }

    public Producto(String nombre, double precio, int cantidadStock, String descripcion, String referencia) {
        this.nombre = nombre;
        this.precio = precio;
        this.cantidadStock = cantidadStock;
        this.descripcion = descripcion;
        this.referencia = referencia;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the precio
     */
    public double getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * @return the cantidadStock
     */
    public int getCantidadStock() {
        return cantidadStock;
    }

    /**
     * @param cantidadStock the cantidadStock to set
     */
    public void setCantidadStock(int cantidadStock) {
        this.cantidadStock = cantidadStock;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the referencia
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * @param referencia the referencia to set
     */
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @Override
    public String toString() {
        return "Producto{" + "nombre=" + nombre + ", precio=" + precio + ", cantidadStock=" + cantidadStock + ", descripcion=" + descripcion + ", referencia=" + referencia + '}';
    }

}