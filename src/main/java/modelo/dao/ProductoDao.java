package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.entity.Producto;
import red.BaseDatos;

/**
 *
 * @author andresparra
 */
public class ProductoDao implements ProductoService {
    public static final String SQL_INSERT = "INSERT INTO producto (nombre, precio, cantidad_stock, descripcion, referencia) VALUES (?,?,?,?,?)";
    public static final String SQL_SELECT = "SELECT * FROM producto";
    public static final String SQL_SELECTID = "SELECT * FROM producto WHERE referencia = ?";
    public static final String SQL_DELETE = "DELETE FROM producto WHERE referencia = ?";
    public static final String SQL_UPDATE = "UPDATE producto";

    @Override
    public void insertar(Producto tienda) {
        
    }

    @Override
    public List<Producto> consultar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Producto producto = null;
        List productos = new ArrayList();
        
        try {
            con = BaseDatos.getConnection();
            ps = con.prepareStatement(SQL_SELECT);
            res = ps.executeQuery();
            while (res.next()) {
                String nombre = res.getString("nombre");
                double precio = res.getDouble("precio");
                int cantidadStock = res.getInt("cantidad_stock");
                String descripcion = res.getString("descripcion");
                String referencia = res.getString("referencia");
                producto = new Producto(nombre, precio, cantidadStock, descripcion, referencia);
                productos.add(producto);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                BaseDatos.close(res);
                BaseDatos.close(ps);
                BaseDatos.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return productos;
    }

    @Override
    public Producto consultarId(Producto producto) {
        return null;
    }

    @Override
    public int borrar(Producto producto) {
       return 0; 
    }

    @Override
    public int actualizar(Producto producto) {
        return 0;
    }
    
}
