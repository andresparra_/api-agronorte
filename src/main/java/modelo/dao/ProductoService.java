package modelo.dao;

import java.util.List;
import modelo.entity.Producto;

/**
 *
 * @author andresparra
 */
public interface ProductoService {
    public void insertar(Producto producto);
    public List<Producto> consultar();
    public Producto consultarId(Producto producto);
    public int borrar(Producto producto);
    public int actualizar(Producto producto);
}
