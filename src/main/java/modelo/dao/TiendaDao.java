/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.entity.Tienda;
import red.BaseDatos;

/**
 *
 * @author IMAGEN-02
 */
public class TiendaDao implements TiendaService {

    public static final String SQL_INSERT = "INSERT INTO tienda (nit,nombre,latitud,longitud,telefono,descripcion) VALUES (?,?,?,?,?,?)";
    public static final String SQL_SELECT = "SELECT * FROM tienda";
    public static final String SQL_SELECTID = "SELECT * FROM tienda WHERE nit = ?";
    public static final String SQL_DELETE = "DELETE FROM tienda WHERE nit = ?";
    public static final String SQL_UPDATE = "UPDATE tienda";

    @Override
    public void insertar(Tienda tienda) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        try {
            con = BaseDatos.getConnection();
            ps = con.prepareStatement(SQL_INSERT);
            ps.setString(1, tienda.getNit());
            ps.setString(2, tienda.getNombre());
            ps.setDouble(3, tienda.getLatitud());
            ps.setDouble(4, tienda.getLongitud());
            ps.setString(5, tienda.getTelefono());
            ps.setString(6, tienda.getDescripcion());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                BaseDatos.close(ps);
                BaseDatos.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<Tienda> consultar() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Tienda tienda = null;
        List tiendas = new ArrayList();
        try {
            con = BaseDatos.getConnection();
            ps = con.prepareStatement(SQL_SELECT);
            res = ps.executeQuery();
            while (res.next()) {
                String nit = res.getString("nit");
                String nombre = res.getString("nombre");
                double latitud = res.getDouble("latitud");
                double longitud = res.getDouble("longitud");
                String telefono = res.getString("telefono");
                String descripcion = res.getString("descripcion");
                tienda = new Tienda(nit, nombre, latitud, longitud, telefono, descripcion);
                tiendas.add(tienda);

            }
        } catch (SQLException ex) {
            Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                BaseDatos.close(res);
                BaseDatos.close(ps);
                BaseDatos.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return tiendas;
    }

    @Override
    public Tienda consultarId(Tienda tienda) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet res = null;
        Tienda tiendaR = null;

        try {
            con = BaseDatos.getConnection();
            ps = con.prepareStatement(SQL_SELECTID);
            ps.setString(1, tienda.getNit());

            res = ps.executeQuery();
   
            if(!res.next()) {
                System.out.println("No hay resultados");
                return null;
            }


            String nit = res.getString("nit");
            String nombre = res.getString("nombre");
            double latitud = res.getDouble("latitud");
            double longitud = res.getDouble("longitud");
            String telefono = res.getString("telefono");
            String descripcion = res.getString("descripcion");

            return new Tienda(nit, nombre, latitud, longitud, telefono, descripcion);


        } catch (SQLException ex) {
            Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                BaseDatos.close(res);
                BaseDatos.close(ps);
                BaseDatos.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return tienda;
    }

    @Override
    public int borrar(Tienda tienda) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;

        try {
            con = BaseDatos.getConnection();
            ps = con.prepareStatement(SQL_DELETE);
            ps.setString(1, tienda.getNit());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                BaseDatos.close(ps);
                BaseDatos.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return registros;
    }

    @Override
    public int actualizar(Tienda tienda) {
        Connection con = null;
        PreparedStatement ps = null;
        int registros = 0;
        
        try {
            con = BaseDatos.getConnection();
            ps = con.prepareStatement(SQL_UPDATE);
            ps.setString(6, tienda.getNit());
            ps.setString(1, tienda.getNombre());
            ps.setDouble(2, tienda.getLatitud());
            ps.setDouble(3, tienda.getLongitud());
            ps.setString(4, tienda.getTelefono());
            ps.setString(5, tienda.getDescripcion());
            registros = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                BaseDatos.close(ps);
                BaseDatos.close(con);
            } catch (SQLException ex) {
                Logger.getLogger(TiendaDao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return registros;
    }

}
