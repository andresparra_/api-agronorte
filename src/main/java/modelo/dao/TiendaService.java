/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package modelo.dao;

import java.util.List;
import modelo.entity.Tienda;

/**
 *
 * @author IMAGEN-02
 */
public interface TiendaService {
    public void insertar(Tienda tienda);
    public List<Tienda> consultar();
    public Tienda consultarId(Tienda tienda);
    public int borrar(Tienda tienda);
    public int actualizar(Tienda tienda);
}
